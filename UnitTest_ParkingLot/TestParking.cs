﻿using System;
using Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ParkingTest
{
    [TestClass]
    public class ParkingTest
    {
        private double interestRate;
        private string expectedHolder;
        private string expectedCurrency;
        private double expectedbalance;

        [TestInitialize]
        public void Init()
        {
            interestRate = 0.1;
            expectedHolder = "Nathan";
            expectedCurrency = "CHF";
            expectedbalance = 9120.45;
        }
        [TestMethod]
        [ExpectedException(typeof(Parkinglot.ParkinglotFull_Expection))]
        public void FullParkingLot_Success()
        {

            //GIVEN
            
            string expectedJsonString = @"{""InterestRate"":0.1,""Balance"":10032.5,""Holder"":""Nathan"",""Currency"":""CHF""}";
            string toJsonString;

            //WHEN
            toJsonString = savings.ToJson();
            //THEN
            Assert.AreEqual(toJsonString, expectedJsonString);
        }

     /*   [TestMethod]
        public void ToJson_BankAccountCreation_Success()
        {

            //GIVEN
            BankAccount bankAccount = new BankAccount(expectedHolder, expectedCurrency, expectedbalance);

            string expectedJsonString = @"{""Holder"":""Nathan"",""Currency"":""CHF"",""Balance"":9120.45}";
            string toJsonString;

            //WHEN
            toJsonString = bankAccount.ToJson();
            //THEN
            Assert.AreEqual(toJsonString, expectedJsonString);
        }
        [TestMethod]
        public void SavingsAccount_BalanceWithInterest_Success()
        {

            //GIVEN
            double balance;
            expectedbalance = 100;
            double expectedInterestBalance = 110;

            SavingsAccount bankAccount = new SavingsAccount(expectedHolder, expectedCurrency, expectedbalance, interestRate);

            //WHEN
            balance = bankAccount.Balance;
            //THEN
            Assert.AreEqual(balance, expectedInterestBalance);
        }
        [TestMethod]
        [ExpectedException(typeof(SavingsAccount.NegativeBalanceException))]
        public void Creating_SavingsAccount_NegativeBalance_Exception()
        {
            //GIVEN
            expectedbalance = -1;
            //WHEN
            //Creation of a new savings account with a negative balance
            SavingsAccount savingsAccount = new SavingsAccount(expectedHolder, expectedCurrency, expectedbalance, interestRate);
        } */
        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            interestRate = 0;
            expectedHolder = "";
            expectedCurrency = "";
            expectedbalance = 0;
        }
    }
}
