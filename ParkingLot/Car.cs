﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
     public  class Car : Vehicle
    {

        #region private attributes
        private string color;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This constructor is designed to 
        /// </summary>
        /// <param name="_size"></param>
        /// <param name="_color"></param>
        public Car(double _size, string _color) : base(_size)
        {
            this.color = _color;
        }
        #endregion constructor

        #region accessors
        public string Color
        {
            get { return color; }
        }
        #endregion accessors

        #region public methods
        /// <summary>
        /// This function is designed to override the ToString method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }
        #endregion public methods
    }
}
