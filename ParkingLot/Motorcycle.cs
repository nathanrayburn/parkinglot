﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Motorcycle : Vehicle
    {

        #region private attributes
        private string brand;
        #endregion private attributes

        #region constructor
        public Motorcycle(double _size, string _brand) : base(_size)
        {
            this.brand = _brand;
        }
        #endregion constructor

        #region accessors
        public string Brand
        {
            get { return brand; }
        }
        #endregion accessors

        #region public methods
        public override string ToString()
        {
            return base.ToString();
        }
        #endregion public methods
    }
}
