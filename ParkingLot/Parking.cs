﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Parkinglot
    {
        #region private attributes
        private double capacity;
        private double vacancy;
        private List<Valet> valets = new List<Valet>();
        private List<Vehicle> parkedVehicles;
        
        #endregion private attrivutes

        #region constructor
        /// <summary>
        /// This constructor is designed to instance a list of vehicles
        /// </summary>
        /// <param name="capacity"></param>
        public Parkinglot(double capacity)
        {
            this.capacity = capacity;

            parkedVehicles = new List<Vehicle>();
         
        }
        #endregion constructor

        #region accessor
        /// <summary>
        /// This accessor is designed to..
        /// </summary>
        public double Vacancy
        {
            get { return vacancy;}            
        }
        #endregion accessor

        #region public method
        /// <summary>
        /// This method is designed to park a new vehicle
        /// </summary>
        /// <param name="_vehicle"></param>
        public void Park(Vehicle _vehicle)
        {
            if (_vehicle.Size > capacity)
            {
                throw new ParkinglotFull_Expection("We don't have any space for this vehicle");
            }
            IsVehicleParked(_vehicle);
            


            Valet _valet =  ChooseValet();

            _valet.Drive(_vehicle);

            parkedVehicles.Add(_vehicle);

            capacity = capacity - _vehicle.Size;

            Console.WriteLine("Vehicle has been successfully parked");
        }
        /// <summary>
        /// This method is designed to ...
        /// </summary>
        /// <param name="_vehicle"></param>
        public void Vacate(Vehicle _vehicle)
        { 
        
        }
        /// <summary>
        /// This method is designed to hire a new valet
        /// </summary>
        /// <param name="_valet"></param>
        public void Hire(Valet _valet)
        {
            Valet valet = new Valet("Jerry");
            valets.Add(valet);
        }
        /// <summary>
        /// This function is designed to fire a valet
        /// </summary>
        /// <param name="_valet"></param>
        public void Fire(Valet _valet)
        {

        }
        #endregion public method

        #region private method

        private bool IsVehicleParked(Vehicle _vehicle) {

            if (parkedVehicles.Contains(_vehicle))
            {
                throw new Exception("This vehicle is already in the parkinglot");
            }

            return false;
        }
        private Valet ChooseValet()
        {
            Valet chosenValet = null;

            foreach (Valet valet in valets)
            {
                if (valet.Available)
                {
                    chosenValet = valet;
                    break;
                }
            }

            if (chosenValet == null)
            {
                throw new Exception("We don't have any available");
            }
            return chosenValet;
        }
        #endregion private method

        /// <summary>
        /// This
        /// </summary>
        public class ParkinglotFull_Expection : Exception
        {
            public ParkinglotFull_Expection(string message) : base(message)
            {
            }
        }
    }
}
