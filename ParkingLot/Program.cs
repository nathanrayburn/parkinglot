﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Park new vehicle\n");
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
            Parkinglot parking = new Parkinglot(100);
            Console.WriteLine("Hire a new valet ");
            Valet valet = new Valet("JErry");
            parking.Hire(valet);
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
            Car genericvehicle = new Car(1,"red");
            parking.Park(genericvehicle);
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }
    }
}
