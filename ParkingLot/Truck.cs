﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Truck : Vehicle
    {
        #region private attributes

        #endregion private attributes

        #region constructor
        public Truck(double _size) : base (_size)
        {

        }
        #endregion constructor

        #region accessors

        #endregion accessors

        #region public methods
        public override string ToString()
        {
            return base.ToString();
        }
        #endregion public methods
    }
}
