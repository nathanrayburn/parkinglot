﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Vehicle
    {
        #region private attributes
        private double size;
        private bool isTurnedOn;
        #endregion private attributes

        #region constructor
        public Vehicle(double _size)
        {
            this.size = _size;
        }
        #endregion constructor

        #region accessors
        public double Size
        {
            get { return size; }
        }
        #endregion accessors

        #region public methods
        public void TurnOn()
        {
            isTurnedOn = true;
        }
        public void Turnoff()
        {
            isTurnedOn = false;
        }
        #endregion public methods
    }
}
