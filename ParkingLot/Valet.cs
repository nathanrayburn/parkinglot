﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Valet
    {
        #region private attributes
        private string name;
        private bool available = true;
        #endregion private attributes

        #region constructor
        public Valet(string _name)
        {
            this.name = _name;
        }
        #endregion constructor

        #region accessors
        public bool Available
        {
            get { return available; }
        }
        #endregion accessors

        #region public methods
        public void Drive(Vehicle _vehicle)
        {
            available = false;
            _vehicle.TurnOn();
            
            Console.Clear();
            
            for (int i = 0; i <= 10;i++)
            {
                Console.WriteLine("Parking the vehicle .. " + i);
                System.Threading.Thread.Sleep(2000);
            }
            _vehicle.Turnoff();

        }
        #endregion public methods
    }
}
